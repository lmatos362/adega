﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Adega
{
    public partial class frmCarrinho : UserControl
    {
        public frmCarrinho()
        {
            InitializeComponent();
        }

        private void label51_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                frmPedir pedir = new frmPedir();
                Parent.Controls.Add(pedir);

                if (Parent.Controls.Count > 1)
                {
                    Parent.Controls.RemoveAt(0);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

           
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoConsultarView> a = business.Consultar(txtPedido.Text);
                dgvCarrinho.AutoGenerateColumns = false;
                dgvCarrinho.DataSource = a;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

         
        }

        private void dgvCarrinho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button18_Click(object sender, EventArgs e)
        {

        }
    }
}
