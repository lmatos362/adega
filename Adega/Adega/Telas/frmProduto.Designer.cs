﻿namespace Adega.Telas
{
    partial class frmProduto
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label40 = new System.Windows.Forms.Label();
            this.btnProduto = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.txtprecocadastro = new System.Windows.Forms.MaskedTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtnomecadastro = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label40.Location = new System.Drawing.Point(-5, 408);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(699, 34);
            this.label40.TabIndex = 115;
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProduto
            // 
            this.btnProduto.BackColor = System.Drawing.Color.Brown;
            this.btnProduto.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.btnProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProduto.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProduto.Location = new System.Drawing.Point(184, 258);
            this.btnProduto.Name = "btnProduto";
            this.btnProduto.Size = new System.Drawing.Size(304, 46);
            this.btnProduto.TabIndex = 134;
            this.btnProduto.Text = "Cadastrar";
            this.btnProduto.UseVisualStyleBackColor = false;
            this.btnProduto.Click += new System.EventHandler(this.btnProduto_Click);
            // 
            // button17
            // 
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.button17.FlatAppearance.BorderSize = 3;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.Brown;
            this.button17.Location = new System.Drawing.Point(184, 322);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(304, 46);
            this.button17.TabIndex = 133;
            this.button17.Text = "Voltar";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label41.Location = new System.Drawing.Point(-11, 35);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(699, 34);
            this.label41.TabIndex = 116;
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtprecocadastro
            // 
            this.txtprecocadastro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprecocadastro.Location = new System.Drawing.Point(270, 213);
            this.txtprecocadastro.Name = "txtprecocadastro";
            this.txtprecocadastro.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtprecocadastro.Size = new System.Drawing.Size(207, 20);
            this.txtprecocadastro.TabIndex = 128;
            this.txtprecocadastro.ValidatingType = typeof(System.DateTime);
            this.txtprecocadastro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecocadastro_KeyPress);
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label43.Location = new System.Drawing.Point(184, 203);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(304, 39);
            this.label43.TabIndex = 127;
            this.label43.Text = "     Preço";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtnomecadastro
            // 
            this.txtnomecadastro.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtnomecadastro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnomecadastro.Location = new System.Drawing.Point(270, 159);
            this.txtnomecadastro.Name = "txtnomecadastro";
            this.txtnomecadastro.Size = new System.Drawing.Size(211, 20);
            this.txtnomecadastro.TabIndex = 117;
            this.txtnomecadastro.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtnomecadastro_MaskInputRejected);
            this.txtnomecadastro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnomecadastro_KeyPress);
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label49.Location = new System.Drawing.Point(184, 150);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(304, 39);
            this.label49.TabIndex = 118;
            this.label49.Text = "    Nome";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label46.Location = new System.Drawing.Point(38, 105);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(603, 37);
            this.label46.TabIndex = 132;
            this.label46.Text = "Coloque os dados do produto abaixo.\r\n";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label50.Location = new System.Drawing.Point(26, 68);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(625, 55);
            this.label50.TabIndex = 131;
            this.label50.Text = "Cadastre aqui o Produto";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.label40);
            this.Controls.Add(this.btnProduto);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.txtprecocadastro);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.txtnomecadastro);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label50);
            this.Name = "frmProduto";
            this.Size = new System.Drawing.Size(683, 477);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnProduto;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.MaskedTextBox txtprecocadastro;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.MaskedTextBox txtnomecadastro;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label50;
    }
}
