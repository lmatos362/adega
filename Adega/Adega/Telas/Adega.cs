﻿using Adega.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adega
{
    public partial class Adega : Form
    {
        public Adega()
        {
            InitializeComponent();
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            if (pnlAdegona.Controls.Count == 1)
            {
                pnlAdegona.Controls.RemoveAt(0);
            }

            frmInicio inicio = new frmInicio();
            pnlAdegona.Controls.Add(inicio);

            pnlMarcador1.Visible = true;
            pnlMarcador2.Visible = false;
            pnlMarcador3.Visible = false;
           
        }

        private void btnCardapio_Click(object sender, EventArgs e)
        {
            if (pnlAdegona.Controls.Count == 1)
            {
                pnlAdegona.Controls.RemoveAt(0);
            }

            frmCardapio cardapio = new frmCardapio();
            pnlAdegona.Controls.Add(cardapio);

            pnlMarcador1.Visible = false;
            pnlMarcador2.Visible = true;
            pnlMarcador3.Visible = false;
            

        }

        private void btnCarrinho_Click(object sender, EventArgs e)
        {
            if (pnlAdegona.Controls.Count == 1)
            {
                pnlAdegona.Controls.RemoveAt(0);
            }

            frmCarrinho carrinho = new frmCarrinho();
            pnlAdegona.Controls.Add(carrinho);

            pnlMarcador1.Visible = false;
            pnlMarcador2.Visible = false;
            pnlMarcador3.Visible = true;
            
        }

        private void btnConta_Click(object sender, EventArgs e)
        {
            if (pnlAdegona.Controls.Count == 1)
            {
                pnlAdegona.Controls.RemoveAt(0);
            }

            frmConta conta = new frmConta();
            pnlAdegona.Controls.Add(conta);

            pnlMarcador1.Visible = false;
            pnlMarcador2.Visible = false;
            pnlMarcador3.Visible = false;
           
        }

        private void btnFechar5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pnlAdegona_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
