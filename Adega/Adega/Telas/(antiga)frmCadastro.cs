﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Adega
{
    public partial class frmCadastro2 : UserControl
    {
        public frmCadastro2()
        {
            InitializeComponent();
        }

        private void btnFechar1_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmLogin2 login = new frmLogin2();
            Parent.Controls.Add(login);

            if (Parent.Controls.Count > 1)
            {
                Parent.Controls.RemoveAt(0);

            }
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            CadastroDTO dto = new CadastroDTO();
            dto.Nome = txtnome.Text;
            dto.Sobrenome = txtsobrenome.Text;
            dto.Cpf = txtcpf.Text;
            dto.Telefone = txttelefono.Text;
            dto.Email = txtemail.Text;
            dto.Senha= txtSenha.Text;

            CadastroBusiness business = new CadastroBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cadastrado com sucesso!.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();   
    }

        private void maskedTextBox5_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
