﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adega.Telas
{
    public partial class frmCadastro : Form

    {
        public char KeyChar { get; set; }

        public frmCadastro()
        {
            InitializeComponent();
        }

        private void btnFechar5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmLogin voltar = new frmLogin();
            voltar.Show();
            Hide();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {

            try
            {
                CadastroDTO dto = new CadastroDTO();
                dto.Nome = txtnome.Text;
                dto.Sobrenome = txtsobrenome.Text;
                dto.Cpf = txtcpf.Text;
                dto.Telefone = txttelefono.Text;
                dto.Email = txtemail.Text;
                dto.Senha = txtSenha.Text;
                dto.ConfSenha = txtconfsenha.Text;


                if (txtnome.Text == "")
                {
                    throw new ArgumentException("Nome obrigatório");
                }

                if (txtsobrenome.Text == "")
                {
                    throw new ArgumentException("Sobrenome obrigatório");
                }

                if (txtemail.Text == "")
                {
                    throw new ArgumentException("Email obrigatório");
                }

                if (txtcpf.Text == "")
                {
                    throw new ArgumentException("CPF obrigatório");
                }

                if (txtSenha.Text == "")
                {
                    throw new ArgumentException("Senha obrigatório");
                }

                if (txtconfsenha.Text == "")
                {
                    throw new ArgumentException("Confirmação da Senha é obrigatório");
                }

                string senha = txtSenha.Text;
                string conf = txtconfsenha.Text;

                if (senha == conf)
                {


                    CadastroBusiness business = new CadastroBusiness();
                    business.Salvar(dto);

                    MessageBox.Show("Cadastrado com sucesso!.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();

                    frmLogin inicio = new frmLogin();
                    inicio.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas (HOJE NÃO BRUNO)", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void txtcpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtnome_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsLetter(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtsobrenome_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtsobrenome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtcpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsDigit(e.KeyChar))
            {
                
                e.Handled = true;
            }
        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(e.KeyChar == (char)Keys.Space))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }

        private void txttelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {

                e.Handled = true;
            }
        }

        private void txtemail_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
