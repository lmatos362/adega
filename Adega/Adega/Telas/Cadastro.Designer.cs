﻿namespace Adega.Telas
{
    partial class frmCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastro));
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txttelefono = new System.Windows.Forms.MaskedTextBox();
            this.txtsobrenome = new System.Windows.Forms.MaskedTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.txtconfsenha = new System.Windows.Forms.MaskedTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.MaskedTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.MaskedTextBox();
            this.lblnome = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFechar5 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label34.Location = new System.Drawing.Point(-10, 30);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(699, 42);
            this.label34.TabIndex = 95;
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label35.Location = new System.Drawing.Point(42, 119);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(603, 37);
            this.label35.TabIndex = 110;
            this.label35.Text = "Só aqui você encontra os rótulos ideais para todos os seus momentos.";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(30, 82);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(625, 55);
            this.label23.TabIndex = 109;
            this.label23.Text = "Comece agora a beber o melhor do vinho!";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txttelefono
            // 
            this.txttelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttelefono.Location = new System.Drawing.Point(443, 186);
            this.txttelefono.Mask = "(99) 00000-0000";
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txttelefono.Size = new System.Drawing.Size(202, 20);
            this.txttelefono.TabIndex = 4;
            this.txttelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelefono_KeyPress);
            // 
            // txtsobrenome
            // 
            this.txtsobrenome.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtsobrenome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsobrenome.Location = new System.Drawing.Point(141, 236);
            this.txtsobrenome.Name = "txtsobrenome";
            this.txtsobrenome.Size = new System.Drawing.Size(181, 20);
            this.txtsobrenome.TabIndex = 1;
            this.txtsobrenome.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtsobrenome_MaskInputRejected);
            this.txtsobrenome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsobrenome_KeyPress);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label33.Location = new System.Drawing.Point(29, 226);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(304, 39);
            this.label33.TabIndex = 112;
            this.label33.Text = "    Sobrenome";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnVoltar
            // 
            this.btnVoltar.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.btnVoltar.FlatAppearance.BorderSize = 3;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.Brown;
            this.btnVoltar.Location = new System.Drawing.Point(30, 385);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(304, 46);
            this.btnVoltar.TabIndex = 108;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // txtconfsenha
            // 
            this.txtconfsenha.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtconfsenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtconfsenha.Location = new System.Drawing.Point(469, 285);
            this.txtconfsenha.Name = "txtconfsenha";
            this.txtconfsenha.PasswordChar = '*';
            this.txtconfsenha.Size = new System.Drawing.Size(176, 20);
            this.txtconfsenha.TabIndex = 6;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label38.Location = new System.Drawing.Point(351, 275);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(304, 39);
            this.label38.TabIndex = 106;
            this.label38.Text = "    Conf. Senha";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSenha
            // 
            this.txtSenha.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenha.Location = new System.Drawing.Point(468, 235);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(176, 20);
            this.txtSenha.TabIndex = 5;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label37.Location = new System.Drawing.Point(351, 226);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(304, 39);
            this.label37.TabIndex = 104;
            this.label37.Text = "        Senha";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(351, 177);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(304, 39);
            this.label21.TabIndex = 103;
            this.label21.Text = "    Telefone";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtemail.Location = new System.Drawing.Point(107, 335);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(216, 20);
            this.txtemail.TabIndex = 3;
            this.txtemail.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtemail_MaskInputRejected);
            this.txtemail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtemail_KeyPress);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(30, 324);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(304, 39);
            this.label20.TabIndex = 102;
            this.label20.Text = "    E-mail";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.BackColor = System.Drawing.Color.Brown;
            this.btnCadastrar.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCadastrar.Location = new System.Drawing.Point(351, 385);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(304, 46);
            this.btnCadastrar.TabIndex = 100;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = false;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // txtcpf
            // 
            this.txtcpf.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtcpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcpf.Location = new System.Drawing.Point(107, 285);
            this.txtcpf.Mask = "000.000.000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(216, 20);
            this.txtcpf.TabIndex = 2;
            this.txtcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtcpf_MaskInputRejected);
            this.txtcpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcpf_KeyPress);
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(30, 275);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(304, 39);
            this.label22.TabIndex = 99;
            this.label22.Text = "    CPF";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtnome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnome.Location = new System.Drawing.Point(110, 186);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(216, 20);
            this.txtnome.TabIndex = 0;
            this.txtnome.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtnome_MaskInputRejected);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_KeyPress);
            // 
            // lblnome
            // 
            this.lblnome.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblnome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblnome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblnome.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblnome.Location = new System.Drawing.Point(29, 177);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(304, 39);
            this.lblnome.TabIndex = 97;
            this.lblnome.Text = "    Nome";
            this.lblnome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Brown;
            this.panel2.Controls.Add(this.btnFechar5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(683, 26);
            this.panel2.TabIndex = 114;
            // 
            // btnFechar5
            // 
            this.btnFechar5.FlatAppearance.BorderSize = 0;
            this.btnFechar5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar5.ForeColor = System.Drawing.Color.Transparent;
            this.btnFechar5.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar5.Image")));
            this.btnFechar5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar5.Location = new System.Drawing.Point(650, 1);
            this.btnFechar5.Name = "btnFechar5";
            this.btnFechar5.Size = new System.Drawing.Size(31, 23);
            this.btnFechar5.TabIndex = 22;
            this.btnFechar5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFechar5.UseVisualStyleBackColor = true;
            this.btnFechar5.Click += new System.EventHandler(this.btnFechar5_Click);
            // 
            // frmCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(683, 477);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txttelefono);
            this.Controls.Add(this.txtsobrenome);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.txtconfsenha);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.btnCadastrar);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblnome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro";
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox txttelefono;
        private System.Windows.Forms.MaskedTextBox txtsobrenome;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.MaskedTextBox txtconfsenha;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.MaskedTextBox txtSenha;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox txtemail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox txtnome;
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFechar5;
    }
}