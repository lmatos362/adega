﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Adega.Telas
{
    public partial class frmProduto : UserControl
    {
        public frmProduto()
        {
            InitializeComponent();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                frmCardapio cardapio = new frmCardapio();
                Parent.Controls.Add(cardapio);

                if (Parent.Controls.Count > 1)
                {
                    Parent.Controls.RemoveAt(0);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

            
        }

        private void btnProduto_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (txtnomecadastro.Text == "")
                {
                    throw new ArgumentException("Nome do produto obrigatório");
                    
                }

                if (txtprecocadastro.Text == "")
                {
                    throw new ArgumentException("Preço do produto obrigatório");
                }

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtnomecadastro.Text;
                dto.Preco = Convert.ToDecimal(txtprecocadastro.Text);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto cadastrado com sucesso.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();

                frmProduto produto = new frmProduto();
                Parent.Controls.Add(produto);

                if (Parent.Controls.Count > 1)
                {
                    Parent.Controls.RemoveAt(0);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
      
        }

        private void txtnomecadastro_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtprecocadastro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {

                e.Handled = true;
            }
        }

        private void txtnomecadastro_KeyPress(object sender, KeyPressEventArgs e)
        {


            if (!char.IsLetter(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
