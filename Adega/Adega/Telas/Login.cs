﻿using Adega.DB.Login;
using Nsf._2018.Modulo2.BD.Musica.DB;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adega.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnFechar5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCadastro_Click(object sender, EventArgs e)
        {
            frmCadastro cadastro = new frmCadastro();
            cadastro.Show();
            Hide();


        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            CadastroBusiness business = new CadastroBusiness();
            CadastroDTO cadastro = business.Logar(maskedTextBox1.Text, maskedTextBox2.Text);
            if ( cadastro != null)
            {
                Class1.Usuariologado = cadastro;
                Adega menu = new Adega();
                menu.Show();
                this.Hide();
               
            }
            else
            {
                MessageBox.Show("Credenciais Inválidas.");
            }
            
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
