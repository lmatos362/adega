﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo2.BD.Musica.DB;

namespace Adega
{
    public partial class frmLogin2 : UserControl
    {
        public frmLogin2()
        {
            InitializeComponent();
        }

        private void btnCadastro_Click(object sender, EventArgs e)
        {

            frmCadastro2 cadastro = new frmCadastro2();
            Parent.Controls.Add(cadastro);

            if (Parent.Controls.Count > 1)
            {
                Parent.Controls.RemoveAt(0);

            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            string email = maskedTextBox1.Text;
            string senha = maskedTextBox2.Text;

            LoginBusiness business = new LoginBusiness();
            bool logou = business.Logar(email, senha);

            if (logou == true)
            {
                frmInicio tela = new frmInicio();
                tela.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas", "Music", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}

