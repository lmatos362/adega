﻿namespace Adega
{
    partial class frmCarrinho
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.btnProcurarCarrinho = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.dgvCarrinho = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Itens = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preço = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarrinho)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(0, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(683, 49);
            this.label9.TabIndex = 100;
            this.label9.Text = "        Nome:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcurarCarrinho
            // 
            this.btnProcurarCarrinho.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProcurarCarrinho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurarCarrinho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcurarCarrinho.Location = new System.Drawing.Point(578, 46);
            this.btnProcurarCarrinho.Name = "btnProcurarCarrinho";
            this.btnProcurarCarrinho.Size = new System.Drawing.Size(98, 29);
            this.btnProcurarCarrinho.TabIndex = 103;
            this.btnProcurarCarrinho.Text = "Procurar";
            this.btnProcurarCarrinho.UseVisualStyleBackColor = false;
            this.btnProcurarCarrinho.Click += new System.EventHandler(this.button19_Click);
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label51.Location = new System.Drawing.Point(0, 412);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(683, 45);
            this.label51.TabIndex = 102;
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label51.Click += new System.EventHandler(this.label51_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(578, 420);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(98, 29);
            this.button4.TabIndex = 101;
            this.button4.Text = "Pedir";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtPedido
            // 
            this.txtPedido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedido.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtPedido.Location = new System.Drawing.Point(109, 46);
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.Size = new System.Drawing.Size(463, 29);
            this.txtPedido.TabIndex = 99;
            // 
            // dgvCarrinho
            // 
            this.dgvCarrinho.AllowUserToAddRows = false;
            this.dgvCarrinho.AllowUserToDeleteRows = false;
            this.dgvCarrinho.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvCarrinho.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCarrinho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCarrinho.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Nome,
            this.Itens,
            this.preço});
            this.dgvCarrinho.Location = new System.Drawing.Point(0, 103);
            this.dgvCarrinho.Name = "dgvCarrinho";
            this.dgvCarrinho.ReadOnly = true;
            this.dgvCarrinho.Size = new System.Drawing.Size(683, 301);
            this.dgvCarrinho.TabIndex = 98;
            this.dgvCarrinho.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCarrinho_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Id";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 40;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Cliente";
            this.Nome.HeaderText = "Cliente";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Itens
            // 
            this.Itens.DataPropertyName = "QtdItens";
            this.Itens.HeaderText = "Itens";
            this.Itens.Name = "Itens";
            this.Itens.ReadOnly = true;
            // 
            // preço
            // 
            this.preço.DataPropertyName = "Total";
            this.preço.HeaderText = "Total";
            this.preço.Name = "preço";
            this.preço.ReadOnly = true;
            // 
            // frmCarrinho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.dgvCarrinho);
            this.Controls.Add(this.txtPedido);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.btnProcurarCarrinho);
            this.Controls.Add(this.label9);
            this.Name = "frmCarrinho";
            this.Size = new System.Drawing.Size(683, 477);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarrinho)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnProcurarCarrinho;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.DataGridView dgvCarrinho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Itens;
        private System.Windows.Forms.DataGridViewTextBoxColumn preço;
    }
}
