﻿namespace Adega
{
    partial class Adega
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adega));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnInsta = new System.Windows.Forms.Button();
            this.btnTwitter = new System.Windows.Forms.Button();
            this.btnFace = new System.Windows.Forms.Button();
            this.pnlMarcador1 = new System.Windows.Forms.Panel();
            this.btnInicio = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pnlMarcador3 = new System.Windows.Forms.Panel();
            this.btnCarrinho = new System.Windows.Forms.Button();
            this.pnlMarcador2 = new System.Windows.Forms.Panel();
            this.btnCardapio = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFechar5 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlAdegona = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.pnlAdegona.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btnInsta);
            this.panel1.Controls.Add(this.btnTwitter);
            this.panel1.Controls.Add(this.btnFace);
            this.panel1.Controls.Add(this.pnlMarcador1);
            this.panel1.Controls.Add(this.btnInicio);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.pnlMarcador3);
            this.panel1.Controls.Add(this.btnCarrinho);
            this.panel1.Controls.Add(this.pnlMarcador2);
            this.panel1.Controls.Add(this.btnCardapio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 492);
            this.panel1.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Maroon;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Location = new System.Drawing.Point(20, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(161, 69);
            this.panel4.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Elephant", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 41);
            this.label1.TabIndex = 9;
            this.label1.Text = "Sardelli";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(7, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 18);
            this.label7.TabIndex = 10;
            this.label7.Text = "Adega Virtual";
            // 
            // btnInsta
            // 
            this.btnInsta.BackColor = System.Drawing.Color.White;
            this.btnInsta.FlatAppearance.BorderSize = 0;
            this.btnInsta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsta.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsta.ForeColor = System.Drawing.Color.White;
            this.btnInsta.Image = ((System.Drawing.Image)(resources.GetObject("btnInsta.Image")));
            this.btnInsta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsta.Location = new System.Drawing.Point(137, 424);
            this.btnInsta.Name = "btnInsta";
            this.btnInsta.Size = new System.Drawing.Size(36, 34);
            this.btnInsta.TabIndex = 11;
            this.btnInsta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnInsta.UseVisualStyleBackColor = false;
            // 
            // btnTwitter
            // 
            this.btnTwitter.BackColor = System.Drawing.Color.White;
            this.btnTwitter.FlatAppearance.BorderSize = 0;
            this.btnTwitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTwitter.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTwitter.ForeColor = System.Drawing.Color.White;
            this.btnTwitter.Image = ((System.Drawing.Image)(resources.GetObject("btnTwitter.Image")));
            this.btnTwitter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTwitter.Location = new System.Drawing.Point(85, 424);
            this.btnTwitter.Name = "btnTwitter";
            this.btnTwitter.Size = new System.Drawing.Size(36, 34);
            this.btnTwitter.TabIndex = 12;
            this.btnTwitter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTwitter.UseVisualStyleBackColor = false;
            // 
            // btnFace
            // 
            this.btnFace.BackColor = System.Drawing.Color.White;
            this.btnFace.FlatAppearance.BorderSize = 0;
            this.btnFace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFace.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFace.ForeColor = System.Drawing.Color.White;
            this.btnFace.Image = ((System.Drawing.Image)(resources.GetObject("btnFace.Image")));
            this.btnFace.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFace.Location = new System.Drawing.Point(33, 424);
            this.btnFace.Name = "btnFace";
            this.btnFace.Size = new System.Drawing.Size(36, 34);
            this.btnFace.TabIndex = 13;
            this.btnFace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFace.UseVisualStyleBackColor = false;
            // 
            // pnlMarcador1
            // 
            this.pnlMarcador1.BackColor = System.Drawing.Color.Brown;
            this.pnlMarcador1.Location = new System.Drawing.Point(-5, 179);
            this.pnlMarcador1.Name = "pnlMarcador1";
            this.pnlMarcador1.Size = new System.Drawing.Size(13, 46);
            this.pnlMarcador1.TabIndex = 7;
            // 
            // btnInicio
            // 
            this.btnInicio.FlatAppearance.BorderSize = 0;
            this.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicio.Font = new System.Drawing.Font("Franklin Gothic Demi", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInicio.Image = ((System.Drawing.Image)(resources.GetObject("btnInicio.Image")));
            this.btnInicio.Location = new System.Drawing.Point(-3, 179);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(224, 46);
            this.btnInicio.TabIndex = 8;
            this.btnInicio.Text = "Inicio";
            this.btnInicio.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInicio.UseVisualStyleBackColor = true;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(200, 26);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(683, 472);
            this.panel6.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(200, 23);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 100);
            this.panel7.TabIndex = 3;
            // 
            // pnlMarcador3
            // 
            this.pnlMarcador3.BackColor = System.Drawing.Color.Brown;
            this.pnlMarcador3.Location = new System.Drawing.Point(-5, 283);
            this.pnlMarcador3.Name = "pnlMarcador3";
            this.pnlMarcador3.Size = new System.Drawing.Size(13, 46);
            this.pnlMarcador3.TabIndex = 5;
            // 
            // btnCarrinho
            // 
            this.btnCarrinho.FlatAppearance.BorderSize = 0;
            this.btnCarrinho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarrinho.Font = new System.Drawing.Font("Franklin Gothic Demi", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarrinho.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCarrinho.Image = ((System.Drawing.Image)(resources.GetObject("btnCarrinho.Image")));
            this.btnCarrinho.Location = new System.Drawing.Point(-3, 283);
            this.btnCarrinho.Name = "btnCarrinho";
            this.btnCarrinho.Size = new System.Drawing.Size(213, 46);
            this.btnCarrinho.TabIndex = 3;
            this.btnCarrinho.Text = "  Carrinho";
            this.btnCarrinho.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCarrinho.UseVisualStyleBackColor = true;
            this.btnCarrinho.Click += new System.EventHandler(this.btnCarrinho_Click);
            // 
            // pnlMarcador2
            // 
            this.pnlMarcador2.BackColor = System.Drawing.Color.Brown;
            this.pnlMarcador2.Location = new System.Drawing.Point(-5, 231);
            this.pnlMarcador2.Name = "pnlMarcador2";
            this.pnlMarcador2.Size = new System.Drawing.Size(13, 46);
            this.pnlMarcador2.TabIndex = 2;
            // 
            // btnCardapio
            // 
            this.btnCardapio.FlatAppearance.BorderSize = 0;
            this.btnCardapio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCardapio.Font = new System.Drawing.Font("Franklin Gothic Demi", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCardapio.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCardapio.Image = ((System.Drawing.Image)(resources.GetObject("btnCardapio.Image")));
            this.btnCardapio.Location = new System.Drawing.Point(-3, 231);
            this.btnCardapio.Name = "btnCardapio";
            this.btnCardapio.Size = new System.Drawing.Size(210, 46);
            this.btnCardapio.TabIndex = 2;
            this.btnCardapio.Text = "   Cardapio";
            this.btnCardapio.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCardapio.UseVisualStyleBackColor = true;
            this.btnCardapio.Click += new System.EventHandler(this.btnCardapio_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Brown;
            this.panel2.Controls.Add(this.btnFechar5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(202, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(681, 26);
            this.panel2.TabIndex = 2;
            // 
            // btnFechar5
            // 
            this.btnFechar5.FlatAppearance.BorderSize = 0;
            this.btnFechar5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar5.ForeColor = System.Drawing.Color.Transparent;
            this.btnFechar5.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar5.Image")));
            this.btnFechar5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar5.Location = new System.Drawing.Point(650, 1);
            this.btnFechar5.Name = "btnFechar5";
            this.btnFechar5.Size = new System.Drawing.Size(31, 23);
            this.btnFechar5.TabIndex = 22;
            this.btnFechar5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFechar5.UseVisualStyleBackColor = true;
            this.btnFechar5.Click += new System.EventHandler(this.btnFechar5_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(682, 466);
            this.panel3.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Maroon;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Location = new System.Drawing.Point(0, 173);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(683, 96);
            this.panel5.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(59, -93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 41);
            this.label4.TabIndex = 27;
            this.label4.Text = "Adega Sardelli";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(83, -32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "Feito com as melhores uvas";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Maroon;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Location = new System.Drawing.Point(19, 15);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(647, 69);
            this.panel8.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Elephant", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(77, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(314, 62);
            this.label2.TabIndex = 26;
            this.label2.Text = "Bem Vindos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(421, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 41);
            this.label3.TabIndex = 9;
            this.label3.Text = "Sardelli";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(443, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Adega Virtual";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(24, -1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 466);
            this.label13.TabIndex = 51;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(99, -4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 466);
            this.label8.TabIndex = 52;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(176, -4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 466);
            this.label9.TabIndex = 53;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(253, -1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 466);
            this.label10.TabIndex = 54;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(329, -1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 466);
            this.label11.TabIndex = 55;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(404, -1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 466);
            this.label12.TabIndex = 56;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(477, -4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 466);
            this.label14.TabIndex = 57;
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(552, -4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 466);
            this.label15.TabIndex = 58;
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(621, -4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 466);
            this.label16.TabIndex = 59;
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlAdegona
            // 
            this.pnlAdegona.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlAdegona.Controls.Add(this.panel3);
            this.pnlAdegona.Location = new System.Drawing.Point(201, 26);
            this.pnlAdegona.Name = "pnlAdegona";
            this.pnlAdegona.Size = new System.Drawing.Size(682, 466);
            this.pnlAdegona.TabIndex = 3;
            this.pnlAdegona.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAdegona_Paint);
            // 
            // Adega
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 492);
            this.Controls.Add(this.pnlAdegona);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Adega";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adega";
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.pnlAdegona.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnInsta;
        private System.Windows.Forms.Button btnTwitter;
        private System.Windows.Forms.Button btnFace;
        private System.Windows.Forms.Panel pnlMarcador1;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel pnlMarcador3;
        private System.Windows.Forms.Button btnCarrinho;
        private System.Windows.Forms.Panel pnlMarcador2;
        private System.Windows.Forms.Button btnCardapio;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFechar5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel pnlAdegona;
    }
}