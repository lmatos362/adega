﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Adega.Telas;
using Nsf._2018.Modulo2.BD.Musica.DB;
using Adega.DB.Login;

namespace Adega
{
    public partial class frmPedir : UserControl
    {
        public frmPedir()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();


        void CarregarCombos()
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoDTO> lista = business.Listar();

                cboProduto.ValueMember = nameof(ProdutoDTO.Id);
                cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
                cboProduto.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message); ;
            }

            
        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            frmCarrinho carrinho = new frmCarrinho();
            Parent.Controls.Add(carrinho);

            if (Parent.Controls.Count > 1)
            {
                Parent.Controls.RemoveAt(0);

            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoDTO dto = new PedidoDTO();
                dto.Data = DateTime.Now;
                

                dto.Id_Cadastro = Class1.Usuariologado.Id;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido salvo com sucesso");
            }


            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }        
        }

        private void label47_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox16_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

                for (int i = 0; i < int.Parse(txtquantidade.Text); i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

           
        }

        private void txtnome_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmPedir_Load(object sender, EventArgs e)
        {

        }

        private void button17_Click_1(object sender, EventArgs e)
        {
            frmCarrinho cardapio = new frmCarrinho();
            Parent.Controls.Add(cardapio);

            if (Parent.Controls.Count > 1)
            {
                Parent.Controls.RemoveAt(0);

            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label40_Click(object sender, EventArgs e)
        {

        }

        private void label41_Click(object sender, EventArgs e)
        {

        }

        private void txtquantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {

                e.Handled = true;
            }
        }
    }
}
