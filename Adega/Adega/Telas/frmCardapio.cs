﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Adega.Telas;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Adega
{
    public partial class frmCardapio : UserControl
    {
        public frmCardapio()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                frmProduto produto = new frmProduto();
                Parent.Controls.Add(produto);

                if (Parent.Controls.Count > 1)
                {
                    Parent.Controls.RemoveAt(0);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

           
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoDTO> lista = business.Consultar(txtProduto.Text);

                dgvProduto.AutoGenerateColumns = false;
                dgvProduto.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

           
        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
