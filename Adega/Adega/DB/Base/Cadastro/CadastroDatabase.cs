﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class CadastroDatabase
    {
        public int Salvar(CadastroDTO dto)
        {
            string script = @"INSERT INTO tb_cadastro (nm_nome, ds_cpf , nm_sobrenome , 
                 ds_telefone, ds_email, ds_senha) 
               VALUES 
              (@nm_nome, @ds_cpf , @nm_sobrenome , @ds_telefone, @ds_email, @ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public CadastroDTO Logar(string usuario, string senha)
        {
            string script =
            @"select *
                FROM tb_cadastro
                WHERE ds_email = @ds_email
                and ds_senha = @ds_senha";
            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("ds_email", usuario));
            parametros.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            CadastroDTO cliente = null;
            if (reader.Read())
            {
                cliente = new CadastroDTO();
                cliente.Id = reader.GetInt32("id_cadastro");
                cliente.Nome = reader.GetString("nm_nome");
                cliente.Sobrenome = reader.GetString("nm_sobrenome");
                cliente.Cpf = reader.GetString("ds_CPF");
                cliente.Telefone = reader.GetString("ds_telefone");
                cliente.Senha = reader.GetString("ds_senha");
                cliente.Email = reader.GetString("ds_email");

            }
            reader.Close();
            return cliente;

        }
    }
}