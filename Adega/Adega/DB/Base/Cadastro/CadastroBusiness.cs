﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class CadastroBusiness
    {
        public int Salvar(CadastroDTO dto)
        {
            CadastroDatabase db = new CadastroDatabase();
            return db.Salvar(dto);
        }
        public CadastroDTO Logar(string email, string senha)
        {
            if (email == string.Empty)
            {
                throw new ArgumentException
                ("Usuário é obrigatório");
            }
            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória");
            }
            CadastroDatabase db = new CadastroDatabase();
            return db.Logar(email, senha);
        }

    }
}
