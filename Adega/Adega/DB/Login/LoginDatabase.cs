﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo2.BD.Musica.DB
{
    public class LoginDatabase
    {
        public bool Logar(string email, string senha)
        {
            string script =
                @"select * 
                    from tb_cadastro
                   where ds_email = @ds_email
                     and ds_senha = @ds_senha";


            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("ds_email", email));
            parametros.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
